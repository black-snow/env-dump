package cmd

import (
	"errors"
)

type stack[T any] []T

func (s *stack[T]) Push(v T) {
	*s = append(*s, v)
}

func (s *stack[T]) Peek() (*T, error) {
	l := len(*s)

	if l == 0 {
		return nil, errors.New("stack is empty")
	}

	last := (*s)[l-1]
	return &last, nil
}

func (s *stack[T]) Pop() (*T, error) {
	popped, err := (*s).Peek()
	if err != nil {
		return nil, err
	}

	*s = (*s)[:len(*s)-1]

	return popped, nil
}

func (s *stack[T]) Empty() bool {
	return len(*s) == 0
}
