FROM --platform=$BUILDPLATFORM golang:1.22@sha256:1a6e657ab55c424c837bd3f18289092caca0a106bcd114a8997b1d7fc81565b0 AS builder

ARG TARGETOS TARGETARCH TARGETPLATFORM

WORKDIR /build

COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .
RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
    CGO_ENABLED=0 GOOS=$TARGETOS GOARCH=$TARGETARCH go build -v -ldflags="-w -s"

FROM --platform=$BUILDPLATFORM scratch

ARG APP_VERSION="0.0.1"

LABEL org.opencontainers.image.licenses="Pacific License" 
LABEL org.opencontainers.image.version=$APP_VERSION
LABEL org.opencontainers.image.url="https://gitlab.com/black-snow/env-dump"

COPY LICENSE /
COPY --from=builder /build/env-dump /build/LICENSE /build/README.md /

ENTRYPOINT [ "/env-dump" ]
CMD [ "--help" ]
