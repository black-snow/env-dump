package main

import (
	"black-snow.net/env-dump/cmd"
)

func main() {
	cmd.Execute()
}
