package cmd

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
)

type KV struct {
	key, value string
}

// var outfile string
var includes []string
var excludes []string
var defaultExcludes [4]string = [4]string{"TERM", "PATH", "HOSTNAME", "HOME"}
var execAfter string
var namespace string
var mappingArg []string

// jsCmd represents the js command
var jsCmd = &cobra.Command{
	Use:   "js outfile",
	Short: "Dump env vars to to js file with window.meta object.",
	Long:  fmt.Sprintf("Dump to js.\nExcludes %s by default unless -e is set, which clears the defaults.", defaultExcludes),
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		dump_to_js(args[0])
	},
}

func init() {
	rootCmd.AddCommand(jsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	//jsCmd.PersistentFlags().StringVar(&outfile, "outfile", "", "File to write to")
	jsCmd.PersistentFlags().StringSliceVarP(&includes, "include", "i", []string{}, "Include given var key instead of taking all env vars. Can be used multiple times or be a comma-separated list")
	jsCmd.PersistentFlags().StringSliceVarP(&excludes, "exclude", "e", defaultExcludes[:], "Exclude given var key. Can be used multiple times or be a comma-separated list")
	jsCmd.PersistentFlags().StringVarP(&execAfter, "exec-afterwards", "a", "", "Execute given command afterwards.")
	jsCmd.PersistentFlags().StringVarP(&namespace, "namespace", "n", "meta", "Where to put the object under window. Doesn't support more levels yet.")
	jsCmd.PersistentFlags().StringArrayVarP(&mappingArg, "map", "m", []string{}, "Map env to another name with optional default. Format: envname=newkey=defaultval or envname=newkey or envname==defaultval")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// jsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func dump_to_js(outfile string) {
	f, err := os.Create(outfile)
	check(err)

	defer f.Close()

	w := bufio.NewWriter(f)
	_, err = w.WriteString(fmt.Sprintf("window.%s = {\"env\": {}};\n", namespace))
	check(err)

	// env name -> new key
	var mappings map[string]string
	// env name -> default value
	var defaults map[string]string
	mappings, defaults = split_mappings(mappingArg)

	// comput key value pairs
	var pairs []KV
	if len(includes) > 0 {
		pairs = get_envs(includes)
	} else {
		pairs = get_all_envs()
	}

	// append defaults
	var env_names map[string]bool = make(map[string]bool, len(pairs))
	for _, kv := range pairs {
		env_names[kv.key] = true
	}
	for env_name, default_value := range defaults {
		_, key_present := env_names[env_name]
		if !key_present {
			pairs = append(pairs, KV{env_name, default_value})
		}
	}

	// write if not excluded
	for _, kv := range pairs {
		if is_excluded(kv.key, excludes) {
			continue
		}

		_, err := write_line(kv.key, kv.value, w, mappings, defaults)
		check(err)
	}

	w.Flush()

	// excute command
	if execAfter != "" {
		cmd, args, err := parse_cmd(execAfter)
		if err != nil {
			check(err)
		}

		output, err := exec.Command(cmd, args...).Output()
		if err != nil {
			fmt.Print(err.Error())
		}

		fmt.Print(string(output))
	}
}

func write_line(key string, value string, w *bufio.Writer, mapping map[string]string, defaults map[string]string) (int, error) {
	// apply mapping
	var key_to_write string
	_, has_mapping := mapping[key]
	if has_mapping {
		key_to_write = mapping[key]
	} else {
		key_to_write = key
	}

	// apply defaults
	var str_val string
	var int_val int64
	var float_val float64
	var bool_val bool
	var is_int, is_float, is_bool error
	default_value, has_default := defaults[key]
	if value == "" && has_default {
		str_val = default_value
		int_val, is_int = strconv.ParseInt(str_val, 10, 32)
		float_val, is_float = strconv.ParseFloat(str_val, 64)
		bool_val, is_bool = strconv.ParseBool(str_val)
	} else {
		str_val = "\"" + value + "\""
		int_val, is_int = strconv.ParseInt(value, 10, 32)
		float_val, is_float = strconv.ParseFloat(value, 64)
		bool_val, is_bool = strconv.ParseBool(value)
	}

	// write
	if is_int == nil {
		return w.WriteString(fmt.Sprintf("window.%s.%s=%d;\n", namespace, key_to_write, int_val))
	} else if is_bool == nil {
		return w.WriteString(fmt.Sprintf("window.%s.%s=%t;\n", namespace, key_to_write, bool_val))
	} else if is_float == nil {
		return w.WriteString(fmt.Sprintf("window.%s.%s=%s;\n", namespace, key_to_write, strconv.FormatFloat(float_val, 'f', -1, 32)))
	} else {
		return w.WriteString(fmt.Sprintf("window.%s.%s=%s;\n", namespace, key_to_write, str_val))
	}
}

func is_excluded(key string, excludes []string) bool {
	for _, e := range excludes {
		if key == e {
			return true
		}
	}
	return len(key) == 0
}

func get_all_envs() []KV {
	envs := os.Environ()
	var pairs []KV = make([]KV, len(envs)-1)
	for _, pair := range envs {
		key, value, _ := strings.Cut(pair, "=")
		pairs = append(pairs, KV{key, value})
	}
	return pairs
}

func get_envs(keys []string) []KV {
	var pairs []KV = make([]KV, len(keys)-1)
	for _, key := range keys {
		value := os.Getenv(key)
		pairs = append(pairs, KV{key, value})
	}
	return pairs
}

// split key=env, key=env=default, key==default into its parts
// returns mapping[env -> new key], defaults[env -> default]
func split_mappings(arg []string) (map[string]string, map[string]string) {
	if len(arg) == 0 {
		return nil, nil
	}

	mapping := make(map[string]string, len(arg))
	defaults := make(map[string]string, len(arg))
	for _, line := range arg {
		env_name, rest, found := strings.Cut(line, "=")
		if !found {
			continue
		}

		k, d, found := strings.Cut(rest, "=")
		if len(k) > 0 {
			mapping[env_name] = k
		}
		if found {
			defaults[env_name] = d
		}
	}
	return mapping, defaults
}

// parse a string into a command and args respecting some level of
// escaping and quoting
func parse_cmd(cmd_str string) (string, []string, error) {
	cmd, args, found := strings.Cut(cmd_str, " ")

	if !found {
		return cmd, []string{}, nil
	}

	// parse char by char to ensure we don't break escaping
	stack := make(stack[rune], 0)
	parsed_args := make([]string, 0)
	current_arg := ""
	for _, c := range args {
		switch c {
		case ' ':
			last, err := stack.Peek()
			if err == nil && *last == '\\' {
				stack.Pop()
				current_arg += string(c)
			} else if err == nil && (*last == '"' || *last == '\'') {
				current_arg += string(c)
			} else {
				if current_arg != " " && len(current_arg) > 0 {
					parsed_args = append(parsed_args, current_arg)
				}
				current_arg = ""
			}
		case '\\':
			last, err := stack.Peek()
			if err == nil && *last == '\\' {
				stack.Pop()
				current_arg += string(c)
			} else {
				stack.Push(c)
			}
		case '"':
			last, err := stack.Peek()
			if err != nil {
				stack.Push(c)
			} else if *last == '\\' {
				current_arg += string(c)
				stack.Pop()
			} else if *last == '"' {
				parsed_args = append(parsed_args, current_arg)
				current_arg = ""
				stack.Pop()
			} else if *last == '\'' {
				current_arg += string(c)
			}
		case '\'':
			last, err := stack.Peek()
			if err != nil {
				stack.Push(c)
			} else if *last == '\\' {
				current_arg += string(c)
				stack.Pop()
			} else if *last == '\'' {
				parsed_args = append(parsed_args, current_arg)
				current_arg = ""
				stack.Pop()
			} else if *last == '"' {
				current_arg += string(c)
			}
		default:
			current_arg += string(c)
		}
	}

	if current_arg != " " && len(current_arg) > 0 {
		parsed_args = append(parsed_args, current_arg)
	}

	if len(stack) > 0 {
		return cmd, parsed_args, errors.New("bad arg string, missing a quote?")
	}

	return cmd, parsed_args, nil
}
