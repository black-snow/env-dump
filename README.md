# env-dump

Dump environment variables into a file.  
Currently supports JavaScript only.

Why would you want this? If you need to inject environment vars
into the file system in minimal images without a shell and without mounting 
anything or sharing the fs/namespace with any other process. 

For example, if you need to apply some magic to env vars and dump
them into a file in a minimal distroless image containing nothing
but nginx to serve some frontend.

Actually, in k8s, you can just mount a config map as a file with
whatever contents you need:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: a-config
  namespace: default
data:
  config.yaml: |
    window.meta = {"env":{}};
    window.meta.yourKey = "whatever value";
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: some-deployment
spec:
  # ...
  template:
    # ...
    spec:
      volumes:
        - name: some-configmap-volume
          configMap:
            name: a-config
    # ...
```

As long as you don't need to inject secrets (mix them in), this should work.

Another solution are init containers but they are slightly slower if
writing env vars to the fs is all you need.

## Building

OCI:
```shell
docker build -t env-dump .
```

Local build:
```shell
go build
```

Multi-arch:
```shell
docker buildx create --name=multi --driver=docker-container --bootstrap --use
docker docker buildx build --platform=linux/arm/v7,linux/arm64/v8,linux/amd64 --builder=multi -t env-dump .
```

## Running

Run with no args or `--help` for details.

Available commands:
* `js` - dump in JavaScript format

Available flags:

| Shorthand | LongOpt | Description |
|---|---|---|
| `-i` | `--include` | Include only given keys instead of all envs. Can be a comma-separated list or set multiple times. |
| `-e` | `--exclude` | Exclude given keys. Can be a comma-separated list or set multiple times. |
| `-a` | `--exec-afterwards` | Run given command after env-dump. |
| `-n` | `--namespace` | What key to use under the `window` object. |
| `-m` | `--map` | Map env to another name with an optional default value. Format: `envname=newkey=defaultvalue` or `envname=newkey` or `envname==defaultvalue` <br> Will include the default if the key is absent even if not listed in `includes`. <br> Respects excludes. |

Note that in order to receive actual strings from default values you'll have to include the
quotes, i.e., `--map "envwithdefault==\"some string\""`. This allows you to set actual 
`undefined`s as well: `--map envwithdefault==undefined`.

Note also that `-a` won't spawn a shell and, for example, `-a 'cat afile | grep -i something'`
won't work.
There's no reason to use `-a` if you have a shell available. There's barely a reason to use 
`env-dump` at all if you've got a shell at hand.

Example: standalone
```shell
./env-dump --help
./env-dump js some.js
./env-dump js some.js -i PATH
./env-dump js some.js -e PATH -e TERM
A=B ./env-dump js some.js -i PATH,A -e SHELL,TERM -a 'cat some.js'
A=B B="" ./env-dump js -i A,B -mA=J --map "B=B=\"test 2\"" -mC=C=7 some.js -a 'cat some.js'
A=B B="" ./env-dump js -i A,B -e C -mA=J --map "B=B=\"test 2\"" -mC=C=7 some.js -a 'cat some.js'
```

Example: all your local env

```shell
docker run -it --name=dump --env-file=<(env) blacksnowflake/env-dump js some.js
docker cp dump:some.js some.js
cat some.js
docker container rm dump
```

Example: use in another image

```shell
docker build -t dumper -f - . <<EOF
FROM alpine:latest

COPY --from=blacksnowflake/env-dump /env-dump /bin/env-dump

CMD ["env-dump", "js", "some.js", "-a", "cat some.js"]
EOF

docker run -it --rm --env-file=<(env) dumper
docker run -it --rm -e A=B -e C=D dumper
# or make env-dump the entrypoint to allow passing args to it
```

> Note: This is an example. Pin your images!

## License

Licensed under [Pacific License](https://github.com/mjmikulski/pacific-license).

Why? I wanted something MIT but *not killing people*.

## 3rd Party Licenses

* [go](https://github.com/golang/go/blob/master/LICENSE) - BSD 3-Clause
* [cobra](https://github.com/spf13/cobra/blob/main/LICENSE.txt) - Apache 2.0
* [pflag](https://github.com/spf13/pflag/blob/master/LICENSE) - BSD 3-Clause
* [mousetrap](https://github.com/inconshreveable/mousetrap/blob/master/LICENSE) - Apache 2.0
