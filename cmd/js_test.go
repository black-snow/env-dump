package cmd

import (
	"fmt"
	"testing"
)

func TestSplitMappingParsesOneEquals(t *testing.T) {
	mappings, defaults := split_mappings([]string{"a=b"})

	assertNotEmpty[string, string](t, mappings, "mapping is empty")
	assertEqual(t, mappings["a"], "b")
	assertEmpty[string, string](t, defaults, "invalid default set")
}

func TestSplitMappingParsesOneEqualsMultipleTImes(t *testing.T) {
	mappings, defaults := split_mappings([]string{"a=b", "c=d"})

	assertNotEmpty[string, string](t, mappings, "mapping is empty")
	assertEqual(t, mappings["a"], "b")
	assertEqual(t, mappings["c"], "d")
	assertEmpty[string, string](t, defaults, "invalid default set")
}

func TestSplitMappingFullExample(t *testing.T) {
	mappings, defaults := split_mappings([]string{
		"a=b",
		"C=D=17",
		"e==undefined",
		"f=g=\"whatever\"",
		"h==true",
		"i==0.3",
	})

	assertNotEmpty[string, string](t, mappings, "mapping is empty")
	assertNotEmpty[string, string](t, defaults, "defaults are empty")

	assertEqual(t, mappings["a"], "b")
	assertEqual(t, mappings["C"], "D")
	assertEqual(t, mappings["f"], "g")
	_, has_key := mappings["e"]
	assertEqual(t, has_key, false)
	_, has_key = mappings["h"]
	assertEqual(t, has_key, false)
	_, has_key = mappings["i"]
	assertEqual(t, has_key, false)

	_, has_key = defaults["a"]
	assertEqual(t, has_key, false)
	assertEqual(t, defaults["C"], "17")
	assertEqual(t, defaults["e"], "undefined")
	assertEqual(t, defaults["f"], "\"whatever\"")
	assertEqual(t, defaults["h"], "true")
	assertEqual(t, defaults["i"], "0.3")
}

func TestSplitMappingEmptyIfNoEq(t *testing.T) {
	mappings, defaults := split_mappings([]string{"a"})
	assertEmpty[string, string](t, mappings, "mapping not empty")
	assertEmpty[string, string](t, defaults, "defaults not empty")
}

func TestSplitMappingDefaultHasEqsIfTooMany(t *testing.T) {
	_, defaults := split_mappings([]string{"a====="})
	assertEqual(t, defaults["a"], "===")
}

func TestParseCmdNoArg(t *testing.T) {
	cmd, args, err := parse_cmd("program")

	assertEqual(t, err, nil)
	assertEqual(t, cmd, "program")
	assertEmpty[string, string](t, args, "args not empty")
}

func TestParseCmdOneArg(t *testing.T) {
	cmd, args, err := parse_cmd("cat a.file")

	assertEqual(t, err, nil)
	assertEqual(t, cmd, "cat", "cmd not parsed")
	assertEqual(t, len(args), 1, "bad number of args parsed")
	assertEqual(t, args[0], "a.file", "bad arg parsed")
}

func TestParseCmdMultipleArgs(t *testing.T) {
	cmd, args, err := parse_cmd("cat a.\\ file > b.file")

	assertEqual(t, err, nil)
	assertEqual(t, cmd, "cat", "cmd not parsed")
	assertEqual(t, len(args), 3, "bad number of args parsed")
	assertEqual(t, args[0], "a. file", "bad first arg")
	assertEqual(t, args[1], ">", "bad second arg")
	assertEqual(t, args[2], "b.file", "bad third arg")
}

func TestParseCmdArgsWithEscape(t *testing.T) {
	cmd, args, err := parse_cmd("cat a\\ b \"another arg\" 'yet another arg'")

	assertEqual(t, err, nil)
	assertEqual(t, cmd, "cat", "cmd not parsed")
	assertEqual(t, len(args), 3, "bad number of args parsed")
	assertEqual(t, args[0], "a b", "bad first arg")
	assertEqual(t, args[1], "another arg", "bad second arg")
	assertEqual(t, args[2], "yet another arg", "bad third arg")
}

func TestParseCmdArgsWithNestedEscape(t *testing.T) {
	cmd, args, err := parse_cmd("cmd \"quote 'inside'\" 'inside \" '")

	assertEqual(t, err, nil)
	assertEqual(t, cmd, "cmd", "cmd not parsed")
	assertEqual(t, len(args), 2, "bad number of args parsed")
	assertEqual(t, args[0], "quote 'inside'", "bad first arg")
	assertEqual(t, args[1], "inside \" ", "bad second arg")
}

func TestParseCmdErrorsOnBadEscape(t *testing.T) {
	_, _, err := parse_cmd("cmd \"missing quote")
	_, _, err2 := parse_cmd("cmd missing quote'")

	assertNotEqual(t, err, nil)
	assertNotEqual(t, err2, nil)
}

// ---

type Lenable interface {
	Len() int
}

func assertEqual(t *testing.T, a interface{}, b interface{}, message ...string) {
	if a == b {
		return
	}

	if len(message) > 0 {
		s := fmt.Sprintf("%s - %v != %v", message[0], a, b)
		t.Fatal(s)
	}
	t.Fatal()
}

func assertNotEqual(t *testing.T, a interface{}, b interface{}, message ...string) {
	if a != b {
		return
	}

	if len(message) > 0 {
		s := fmt.Sprintf("%s - %v == %v", message[0], a, b)
		t.Fatal(s)
	}
	t.Fatal()
}

func assertNotEmpty[T any, C comparable](t *testing.T, actual interface{}, message string) {
	switch actual_type := actual.(type) {
	case string:
		if len(actual_type) <= 0 {
			t.Fatal(message)
		}
	case []interface{}:
		if len(actual_type) <= 0 {
			t.Fatal(message)
		}
	case []T:
		if len(actual_type) <= 0 {
			t.Fatal(message)
		}
	case map[C]T:
		if len(actual_type) <= 0 {
			t.Fatal(message)
		}
	case chan int:
		if len(actual_type) <= 0 {
			t.Fatal(message)
		}
	default:
		t.Fatalf(`%v is not 'len-able'`, actual)
	}
}

func assertEmpty[T any, C comparable](t *testing.T, actual interface{}, message string) {
	switch actual_type := actual.(type) {
	case string:
		if len(actual_type) != 0 {
			t.Fatal(message)
		}
	case []interface{}:
		if len(actual_type) != 0 {
			t.Fatal(message)
		}
	case []T:
		if len(actual_type) != 0 {
			t.Fatal(message)
		}
	case map[C]T:
		if len(actual_type) != 0 {
			t.Fatal(message)
		}
	case chan int:
		if len(actual_type) != 0 {
			t.Fatal(message)
		}
	default:
		t.Fatalf(`%v is not 'len-able'`, actual)
	}
}
