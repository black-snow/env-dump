package cmd

import (
	"testing"
)

func TestStackPushPop(t *testing.T) {
	s := make(stack[int], 0)
	s.Push(13)
	popped, _ := s.Pop()
	_, err := s.Pop()

	assertEqual(t, true, s.Empty())
	assertEqual(t, *popped, 13, "didn't pop 13")
	assertNotEqual(t, err, nil, "no error popped from empty stack")
}

func TestStackPeek(t *testing.T) {
	s := make(stack[int], 0)

	_, err := s.Peek()
	s.Push(13)
	peeked, _ := s.Peek()

	assertNotEqual(t, err, nil, "no error peeked from empty stack")
	assertEqual(t, *peeked, 13, "didn't peek 13")
	assertEqual(t, 1, len(s), "peek didn't leave stack untouched")
	assertEqual(t, false, s.Empty())
}
